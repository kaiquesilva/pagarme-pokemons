const assert = require('assert');
const app = require('../../src/app');

describe('\'buy-pokemon\' service', () => {
  it('registered the service', () => {
    const service = app.service('buy-pokemon');

    assert.ok(service, 'Registered the service');
  });
});
