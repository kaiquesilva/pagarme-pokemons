// Initializes the `buy-pokemon` service on path `/buy-pokemon`
const createService = require('feathers-sequelize');
const createModel = require('../../models/buy-pokemon.model');
const hooks = require('./buy-pokemon.hooks');
const filters = require('./buy-pokemon.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'buy-pokemon',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/buy-pokemon', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('buy-pokemon');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
