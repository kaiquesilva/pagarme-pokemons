// Initializes the `pokemons` service on path `/pokemons`
const createService = require('feathers-sequelize');
const createModel = require('../../models/pokemons.model');
const hooks = require('./pokemons.hooks');
const filters = require('./pokemons.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'pokemons',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/pokemons', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('pokemons');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
