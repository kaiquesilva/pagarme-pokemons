const pokemons = require('./pokemons/pokemons.service.js');
const buyPokemon = require('./buy-pokemon/buy-pokemon.service.js');
module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(pokemons);
  app.configure(buyPokemon);
};
