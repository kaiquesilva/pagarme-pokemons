// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const buyPokemon = sequelizeClient.define('buy-pokemon', {
    pokemonId: {
      type: Sequelize.DATE,
      allowNull: false
    },
    //transaction: {
    //  type: Sequelize.STRING,
    //  allowNull: true 
    //},
    buyedAt: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    },
  }, {
    classMethods: {
      associate (models) { // eslint-disable-line no-unused-vars
        // Define associations here
        // See http://docs.sequelizejs.com/en/latest/docs/associations/
      }
    }
  });

  return buyPokemon;
};
