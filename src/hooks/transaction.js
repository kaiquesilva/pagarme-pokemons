// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
const superagent = require('superagent');

module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return function (hook,next) {
    // Hooks can either return nothing or a promise
    // that resolves with the `hook` object for asynchronous operations
    let pokemonService = (hook.app !== undefined ? hook.app.service('pokemons') : null);
    if (hook.data !== undefined && (pokemonService !== null && (pokemonService.get !== undefined))) {
      pokemonService
	.get(parseInt(hook.data.pokemonId,10),{query:{}})
        .then((pk) => {
          superagent
            .post(`https://api.pagar.me/${hook.data.pokemonId}/transactions`)
            .send({
              api_key: 'ak_test_WHgSu2XFmvoopAZMetV3LfA2RfEEQg',
              amount: pk.price * (hook.data.quantity !== undefined ? hook.data.quantity:1) * 100,
              card_number: '4024007138010896',
              card_expiration_date: '1050',
              card_holder_name: 'Ash Ketchum',
              card_cvv: '123',
              metadata: { product: 'Pokemon', name: pk.name, quantity: (hook.data.quantity !== undefined ? hook.data.quantity:1) },
            })
            .end((err, res) => {
              hook.result.transaction = res.body;
              next();
              return Promise.resolve(hook);
            });
        });
    } else {
      return Promise.resolve(hook);
    }
  };
};
